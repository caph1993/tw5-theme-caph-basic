Caph Basic = Basic tiddlywiki theme + dark/light mode + narrow/wide mode

Caph Basic is a TiddlyWiki theme. It is an extension of the default Basic Theme Vanilla.

The light color scheme is the usual vanilla and the dark is inspired on SublimeText 3, the dark theme of StackOverflow and my personal taste and css skills. The read mode forces around 66 characters per line, which is good for read focus.

Install by opening the html demo file, clicking on 'Theme', and dragging the plugin caph-basic to your TiddlyWiki. Use {{$:/themes/caph/basic/quick-settings}} in any of your tiddlers to show the quick settings. It can be used on servers as well because the user toggle states are saved in $:/states.

Requires:
 + Codemirror plugin
 + Highlight.js plugin
 + Basic Theme vanilla (installed by default)
 + Browser storage (installed by default on TW5.1.21). Not mandatory, but allows modes to persist after page reloading.
